defmodule Identicon do
  @moduledoc """
  Documentation for `Identicon`.
  """
  def main(input) do
input
|> hash_input()
|> pick_color()
|> build_grid()
|> filter_odd_squares()
|> build_pixel_map()
|> draw_image()
|> save_image(input)
  end
  @doc """
    Guarda la imagen generada en formato `png` en la carpeta del proyecto
  """
  def save_image(image, input) do
    File.write("#{input}.png",image)
  end

  @doc """
    Dibuja `en memoria` el identicon
  """
  def draw_image(%Identicon.Image{color: color, pixel_map: pixel_map}) do
    image = :egd.create(250, 250)
    fill = :egd.color(color)
    Enum.each(pixel_map, fn({start, stop}) ->
      :egd.filledRectangle(image,start,stop,fill)
    end)
    :egd.render(image)
  end

  def build_pixel_map(%Identicon.Image{grid: grid} = image) do
    pixel_map = Enum.map grid, fn({_code, index}) ->
      horizontal = rem(index, 5) * 50
      vertical = div(index, 5) * 50
      top_left = {horizontal, vertical}
      bottom_rigth = {horizontal + 50, vertical + 50}
      {top_left,bottom_rigth}
    end
    %Identicon.Image{image | pixel_map: pixel_map}
  end

  @doc """
    Genera un código unico `MD5` en base al texto ingresado
  """
  def hash_input(input) do
   hex = :crypto.hash(:md5,input)
   |>:binary.bin_to_list()
   %Identicon.Image{hex: hex}
  end

  @doc """
    Genera el color a usarse, en base a los primeros 3 números del MD5
  """
  def pick_color(%Identicon.Image{hex: [r,g,b | _tail]} = image) do
    %Identicon.Image{image | color: {r,g,b}}
  end

  @doc """
    Obtiene una lista de los sectores del grid que contienen un valor
    divisible entre 2, ellos serán pintados.
  """
  def filter_odd_squares(%Identicon.Image{grid: grid} = image) do
    grid = Enum.filter grid, fn({code,_index}) ->
      rem(code, 2) == 0
    end
    %Identicon.Image{image | grid: grid}
  end

  @doc """
    Genera tuplas de 3 , luego duplica los primeros 2 números para que el
    identicon sea simetrico y despues les asigna un indice para identificar
    cada número en el grid.
  """
  def build_grid(%Identicon.Image{hex: hex} = image) do
    grid =
      hex
      |> Enum.chunk(3)
      |> Enum.map(&mirror_row/1)
      |> List.flatten()
      |> Enum.with_index()
      %Identicon.Image{image | grid: grid}
  end

  @doc """
    Duplica los primeros 2 digitos de una tupla de 3 digitos.
  """
  def mirror_row(row) do
    [first, second | _tail] = row
    row ++ [second, first]
  end

end
