defmodule Palindrome do
  @moduledoc """
  Documentation for `Palindrome`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Palindrome.hello()
      :world

  """
  def hello do
    :world
  end
end
