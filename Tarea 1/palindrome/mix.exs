defmodule Palindrome.MixProject do
  use Mix.Project

  def project do
    [
      app: :palindrome,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end

  def is_palindrome(word) when is_binary(word) do
    String.to_char_list(word)
    |> reverse
    |> is_palindrome(word)
  end

  def is_palindrome(word) when is_list(word) do
    word
    |> reverse
    |> is_palindrome(word)
  end

  def is_palindrome(number) when is_integer(number) do
    Integer.to_string(number)
    |> reverse
    |> is_palindrome
  end

  def is_palindrome do
    IO.puts "Please enter a word"
  end

  defp reverse([head|tail]) do
    reversed_word = [head]
    reverse(reversed_word, tail)
  end

  defp reverse(reversed_word, [head|tail]) do
    reverse([head] ++ [reversed_word], tail)
  end

  defp reverse(reversed_word, []) do
    List.to_string reversed_word
  end

  defp is_palindrome(reversed_word, word) when word == reversed_word do
   IO.puts "#{word} IS a palindrome"
  end

  defp is_palindrome(reversed_word, word) when word != reversed_word do
  IO.puts "#{word} IS NOT a palindrome"
  end
end
