defmodule Flatten do
  @moduledoc """
  Documentation for `Flatten`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Flatten.hello()
      :world

  """
  def hello do
    :world
  end
end
