defmodule Flatten.MixProject do
  use Mix.Project

  def project do
    [
      app: :flattenApp,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end

  def flatten(array, flag \\ :uniq) do
    case flag do
      :uniq ->
        array
        |> flat
        |> uniq

      :no_uniq ->
        array |> flat

      _ ->
        array |> flat
    end
  end

  defp flat([head | tail]) when is_list(head) do
    flat(head) ++ flat(tail)
  end

  defp flat([head | tail]) do
    [head | flat(tail)]
  end

  defp flat(item), do: item

  def uniq(list) do
    uniq(list, MapSet.new())
  end

  def uniq([item | rest], list) do
    if MapSet.member?(list, item) do
      uniq(rest, list)
    else
      [item | uniq(rest, MapSet.put(list, item))]
    end
  end

  def uniq([], _) do
    []
  end
end
