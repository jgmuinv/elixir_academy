defmodule Proyect.Empleado do
  use Proyect.Web, :model

  schema "empleados" do
    field :nombre, :string
    field :edad, :integer
    field :sexo, :string
    field :profesion, :string
    field :sueldo, :decimal
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:nombre, :edad, :sexo, :profesion, :sueldo])
    |> validate_required([:nombre, :edad, :sexo, :profesion, :sueldo])
    |> validate_inclusion(:edad, 18..65)
    |> validate_number(:sueldo, greater_than: 0)
    |> validate_length(:sexo, is: 1)
    |> validate_inclusion(:sexo, ["M", "F"])
  end
end
