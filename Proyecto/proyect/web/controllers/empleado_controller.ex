defmodule Proyect.EmpleadoController do
  use Proyect.Web, :controller
  alias Proyect.Empleado

  def new(conn, _params) do
    changeset = Empleado.changeset(%Empleado{}, %{})

    render conn, "new.html", changeset: changeset
  end

  def create(conn, %{"empleado" => empleado}) do
    changeset = Empleado.changeset(%Empleado{}, empleado)
    case Repo.insert(changeset) do
      {:ok, _empleado} ->
        conn
        |> put_flash(:info,"Empleado ingresado")
        |> redirect(to: empleado_path(conn, :index))
      {:error, changeset} ->
        render conn, "new.html", changeset: changeset
    end
  end

  def nombres(conn, _params) do
    empleados = Repo.all(Empleado)
    render conn, "nombres.html", empleados: empleados
  end

  def sexos(conn, _params) do
    hombresSQL = from p in "empleados", where: p.sexo == "M"
    hombres = Repo.aggregate(hombresSQL, :count, :sexo)
    mujeresSQL = from p in "empleados", where: p.sexo == "F"
    mujeres = Repo.aggregate(mujeresSQL, :count, :sexo)
    render conn, "sexos.html", hombres: hombres, mujeres: mujeres
  end

  def profesiones(conn, _params) do
    profesionesSQL = from p in "empleados",  group_by: [p.profesion], select: %{prof: p.profesion, num: count(p.id)}
    profesiones = Repo.all(profesionesSQL)
    render conn, "profesiones.html", profesiones: profesiones
  end

  def presupuestos(conn, _params) do
    presupuestoSQL = from p in "empleados", select: %{pre: sum(p.sueldo)}
    presupuesto = Repo.all(presupuestoSQL)
    IO.inspect("+++++++++++")
    IO.inspect(presupuesto)
    render conn, "presupuestos.html", presupuesto: presupuesto
  end

  def edades(conn, _params) do
    empleados = Repo.all(Empleado)
    render conn, "edades.html", empleados: empleados
  end

  def sueldos(conn, _params) do
    empleados = Repo.all(Empleado)
    render conn, "sueldos.html", empleados: empleados
  end

  def index(conn, _params) do
    empleados = Repo.all(Empleado)
    render conn, "index.html", empleados: empleados
  end

  def edit(conn, %{"id" => empleado_id}) do
    empleado = Repo.get(Empleado, empleado_id)
    changeset = Empleado.changeset(empleado)
    render conn, "edit.html", changeset: changeset, empleado: empleado
  end

  def update(conn, %{"id" => empleado_id, "empleado" => empleado}) do
    old_empleado = Repo.get(Empleado, empleado_id)
    changeset = Repo.get(Empleado,empleado_id) |> Empleado.changeset(empleado)
    case Repo.update(changeset) do
      {:ok,_topic} ->
        conn
        |> put_flash(:info, "Empleado actualizado")
        |> redirect(to: empleado_path(conn, :index))
      {:error, changeset} ->
        render conn, "edit.html", changeset: changeset, empleado: old_empleado
    end
  end

  def delete(conn, %{"id" => empleado_id}) do
      Repo.get!(Empleado, empleado_id) |> Repo.delete!
      conn
      |> put_flash(:info, "Empleado borrado")
      |> redirect(to: empleado_path(conn, :index))
  end
end
