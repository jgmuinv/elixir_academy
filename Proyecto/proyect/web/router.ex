defmodule Proyect.Router do
  use Proyect.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Proyect do
    pipe_through :browser # Use the default browser stack
  get "/nombres", EmpleadoController, :nombres
  get "/edades", EmpleadoController, :edades
  get "/sueldos", EmpleadoController, :sueldos
  get "/sexos", EmpleadoController, :sexos
  get "/profesiones", EmpleadoController, :profesiones
  get "/presupuestos", EmpleadoController, :presupuestos
  #  get "/", EmpleadoController, :index
  #  get "/empleados/new", EmpleadoController, :new
  #  get "/empleados/:id/edit", EmpleadoController, :edit
  #  post "/empleados", EmpleadoController, :create
  #  put "/empleados/:id", EmpleadoController, :update
  #  delete "/empleados/:id", EmpleadoController, :delete
  resources "/", EmpleadoController
  end

  # Other scopes may use custom stacks.
  # scope "/api", Proyect do
  #   pipe_through :api
  # end
end
